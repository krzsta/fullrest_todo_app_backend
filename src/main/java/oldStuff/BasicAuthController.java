package oldStuff;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class BasicAuthController {

    @GetMapping("/basicauth")
    public BasicAuthenticationService auth() {
        return new BasicAuthenticationService("message");
    }
}
