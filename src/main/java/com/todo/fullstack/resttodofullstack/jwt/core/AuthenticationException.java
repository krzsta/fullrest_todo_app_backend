package com.todo.fullstack.resttodofullstack.jwt.core;

public class AuthenticationException extends RuntimeException {
    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
}
