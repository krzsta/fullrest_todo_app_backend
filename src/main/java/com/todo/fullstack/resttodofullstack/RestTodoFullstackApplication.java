package com.todo.fullstack.resttodofullstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestTodoFullstackApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestTodoFullstackApplication.class, args);
    }

}
