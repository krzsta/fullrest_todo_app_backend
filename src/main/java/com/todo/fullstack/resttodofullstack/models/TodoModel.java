package com.todo.fullstack.resttodofullstack.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
public class TodoModel {

    @Id
    @GeneratedValue
    private Long id;
    private String username;
    private String desc;
    private Date targetDate;
    private boolean isDone;
}
