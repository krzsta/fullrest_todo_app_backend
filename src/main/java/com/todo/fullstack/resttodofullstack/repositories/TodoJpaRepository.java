package com.todo.fullstack.resttodofullstack.repositories;

import com.todo.fullstack.resttodofullstack.models.TodoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoJpaRepository extends JpaRepository<TodoModel, Long> {

    List<TodoModel> findByUsername(String username);
}
