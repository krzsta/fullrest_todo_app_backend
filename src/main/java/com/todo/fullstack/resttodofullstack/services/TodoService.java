package com.todo.fullstack.resttodofullstack.services;

import com.todo.fullstack.resttodofullstack.models.TodoModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TodoService {
    private static List<TodoModel> todoModelsList = new ArrayList<>();
    private static long idCounter = 0;

    static {
        todoModelsList.add(new TodoModel(++idCounter, "Krzysiek", "Krzysiek desc", new Date(), false));
        todoModelsList.add(new TodoModel(++idCounter, "Maja", "Maja desc", new Date(), false));
        todoModelsList.add(new TodoModel(++idCounter, "Borys", "Borys desc", new Date(), false));
        todoModelsList.add(new TodoModel(++idCounter, "Babe", "Babe desc", new Date(), false));
    }

    public List<TodoModel> findAll() {
        return todoModelsList;
    }

    public TodoModel saveTodo(TodoModel todoModel) {
        if (todoModel.getId() == -1 || todoModel.getId() == 0) {
            todoModel.setId(++idCounter);
        } else {
            deleteById(todoModel.getId());
        }
        todoModelsList.add(todoModel);
        return todoModel;
    }

    public TodoModel deleteById(long id) {
        TodoModel todoModel = findById(id);
        if (todoModel == null) return null;
        if (todoModelsList.remove(todoModel)) {
            return todoModel;
        }
        return null;
    }

    public TodoModel findById(long id) {
        for (TodoModel todoModel : todoModelsList) {
            if (todoModel.getId() == id) {
                return todoModel;
            }
        }
        return null;
    }
}
