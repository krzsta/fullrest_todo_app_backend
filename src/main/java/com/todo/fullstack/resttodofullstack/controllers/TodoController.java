package com.todo.fullstack.resttodofullstack.controllers;

import com.todo.fullstack.resttodofullstack.models.TodoModel;
import com.todo.fullstack.resttodofullstack.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin()
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping("/users/{username}/todos")
    public List<TodoModel> getAllTodo(@PathVariable String username) {
        return todoService.findAll();
    }

    @GetMapping("/users/{username}/todos/{id}")
    public TodoModel getTodoById(@PathVariable String username, @PathVariable long id) {
        return todoService.findById(id);
    }

    @PutMapping("/users/{username}/todos/{id}")
    public ResponseEntity<TodoModel> updateTodo(@PathVariable String username, @PathVariable long id, @RequestBody TodoModel todoModel) {
        TodoModel updatedTodoModel = todoService.saveTodo(todoModel);
        return new ResponseEntity<>(updatedTodoModel, HttpStatus.OK);
    }

    @PostMapping("/users/{username}/todos/")
    public ResponseEntity<Void> addTodo(@PathVariable String username, @RequestBody TodoModel todoModel) {
        TodoModel createdTodoModel = todoService.saveTodo(todoModel);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdTodoModel.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/users/{username}/todos/{id}")
    public ResponseEntity<Void> deleteTodo(@PathVariable String username, @PathVariable long id) {
        TodoModel todoModel = todoService.deleteById(id);

        return todoModel != null ? ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
    }
}
