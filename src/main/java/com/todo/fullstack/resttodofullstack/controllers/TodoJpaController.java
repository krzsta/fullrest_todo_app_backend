package com.todo.fullstack.resttodofullstack.controllers;

import com.todo.fullstack.resttodofullstack.models.TodoModel;
import com.todo.fullstack.resttodofullstack.repositories.TodoJpaRepository;
import com.todo.fullstack.resttodofullstack.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin()
public class TodoJpaController {

    @Autowired
    private TodoService todoService;

    @Autowired
    private TodoJpaRepository todoJpaRepository;

    @GetMapping("/jpa/users/{username}/todos")
    public List<TodoModel> getAllTodo(@PathVariable String username) {
        return todoJpaRepository.findByUsername(username);
    }

    @GetMapping("/jpa/users/{username}/todos/{id}")
    public TodoModel getTodoById(@PathVariable String username, @PathVariable long id) {
        return todoJpaRepository.findById(id).get();
    }

    @PutMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<TodoModel> updateTodo(@PathVariable String username, @PathVariable long id, @RequestBody TodoModel todoModel) {
        todoModel.setUsername(username);
        TodoModel updatedTodoModel = todoJpaRepository.save(todoModel);

        return new ResponseEntity<>(updatedTodoModel, HttpStatus.OK);
    }

    @PostMapping("/jpa/users/{username}/todos/")
    public ResponseEntity<Void> createTodo(@PathVariable String username, @RequestBody TodoModel todoModel) {
        TodoModel createdTodoModel = todoJpaRepository.save(todoModel);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdTodoModel.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<Void> deleteTodo(@PathVariable String username, @PathVariable long id) {
        todoJpaRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
